﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Timers;
using TaskTimerModel.Interfaces;

namespace TaskTimerModel
{
    public class TaskManager : ITaskManager
    {
        public List<Task> TaskList { get; protected set; }
        public int ActiveTaskId { get; protected set; }
        public long TimerStartTime { get; protected set; }
        public bool isRunning { get; protected set; }
        public EventHandler Update { get; set; }

        public TimeSpan TotalTimeElapsed
        {
            get
            {
                long elapsedTime = DateTimeOffset.Now.ToUnixTimeSeconds() - TimerStartTime;
                return DateTimeOffset.FromUnixTimeSeconds(elapsedTime).TimeOfDay;
            }
        }

        public TaskManager()
        {
            TaskList = new List<Task>();
            ActiveTaskId = -1;
            InitializeTimer();
        }

        private Timer timer;

        private void InitializeTimer()
        {
            timer = new Timer(1000);
            timer.Elapsed += InvokeUpdate;
        }

        public void AddTask(Task task)
        {
            TaskList.Add(task);
        }

        public void Log(string path)
        {
            throw new NotImplementedException();
        }

        public void RemoveTask(int id)
        {
            Task task = TaskList.Find(x => x.Id == id);
            if (task != null)
            {
                TaskList.Remove(task);
            }
        }

        public void ResetTimer()
        {
            TimerStartTime = DateTimeOffset.Now.ToUnixTimeSeconds();
            StopTimer();
        }

        public void SetActiveTask(int id)
        {
            int index = TaskList.FindIndex(x => x.Id == id);
            if (index >= 0)
            {
                ActiveTaskId = id;
            }
        }

        public void StartTimer()
        {
            TimerStartTime = DateTimeOffset.Now.ToUnixTimeSeconds();
            isRunning = true;
            if (timer == null)
            {
                InitializeTimer();
            }
            timer?.Start();
        }

        private void InvokeUpdate(object sender, ElapsedEventArgs e)
        {
            Update?.Invoke(this, e);
        }

        public void StopTimer()
        {
            isRunning = false;
            timer?.Stop();
        }


    }
}
