﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskTimerModel
{
    public class Task
    {
        public int Id { get; protected set; }
        public string Name { get; set; }
        public long OverallDuration { get; set; }

        public Task()
        {
            Id = ID++;
        }

        private static int ID = 0;
    }
}
