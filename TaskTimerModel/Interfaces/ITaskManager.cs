﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskTimerModel.Interfaces
{
    public interface ITaskManager
    {
        List<Task> TaskList { get; }
        int ActiveTaskId { get; }
        long TimerStartTime { get; }
        TimeSpan TotalTimeElapsed { get; }
        EventHandler Update { get; set; }

        void StartTimer();
        void StopTimer();
        void ResetTimer();
        void Log(string path);
        void AddTask(Task task);
        void RemoveTask(int id);
        void SetActiveTask(int id);
    }
}
