﻿using System;
using System.Windows.Forms;
using TaskTimerPresenter;
using TaskTimerModel;

namespace TaskTimer
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        [STAThread]
        static void Main()
        {
            Application.EnableVisualStyles();
            Application.SetCompatibleTextRenderingDefault(false);
            
            TaskManager taskManager = new TaskManager();
            MainView mainView = new MainView();
            
            taskManager.AddTask(new TaskTimerModel.Task() { Name = "Task1" });
            taskManager.AddTask(new TaskTimerModel.Task() { Name = "Task2" });
            taskManager.AddTask(new TaskTimerModel.Task() { Name = "Task3" });
            taskManager.AddTask(new TaskTimerModel.Task() { Name = "Task4" });
            taskManager.SetActiveTask(2);
            
            MainViewPresenter mainViewPresenter = new MainViewPresenter(mainView, taskManager);
            
            Application.Run(mainView);
        }
    }
}
