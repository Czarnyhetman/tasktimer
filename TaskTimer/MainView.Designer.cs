﻿namespace TaskTimer
{
    partial class MainView
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.textBox_TotalElapsedTime = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.button_StartTimer = new System.Windows.Forms.Button();
            this.button_StopTimer = new System.Windows.Forms.Button();
            this.button_ResetTimer = new System.Windows.Forms.Button();
            this.button_Log = new System.Windows.Forms.Button();
            this.button_Settings = new System.Windows.Forms.Button();
            this.panel_TaskList = new System.Windows.Forms.Panel();
            this.tableLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // textBox_TotalElapsedTime
            // 
            this.textBox_TotalElapsedTime.BackColor = System.Drawing.Color.White;
            this.textBox_TotalElapsedTime.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.textBox_TotalElapsedTime.Dock = System.Windows.Forms.DockStyle.Top;
            this.textBox_TotalElapsedTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 36.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox_TotalElapsedTime.Location = new System.Drawing.Point(0, 0);
            this.textBox_TotalElapsedTime.Name = "textBox_TotalElapsedTime";
            this.textBox_TotalElapsedTime.ReadOnly = true;
            this.textBox_TotalElapsedTime.Size = new System.Drawing.Size(411, 62);
            this.textBox_TotalElapsedTime.TabIndex = 0;
            this.textBox_TotalElapsedTime.Text = "00:00:00";
            this.textBox_TotalElapsedTime.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Controls.Add(this.button_StartTimer, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.button_StopTimer, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.button_ResetTimer, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.button_Log, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.button_Settings, 4, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Top;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 62);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(411, 84);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // button_StartTimer
            // 
            this.button_StartTimer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_StartTimer.Location = new System.Drawing.Point(3, 3);
            this.button_StartTimer.Name = "button_StartTimer";
            this.button_StartTimer.Size = new System.Drawing.Size(76, 78);
            this.button_StartTimer.TabIndex = 0;
            this.button_StartTimer.Text = "Start";
            this.button_StartTimer.UseVisualStyleBackColor = true;
            this.button_StartTimer.Click += new System.EventHandler(this.button_StartTimer_Click);
            // 
            // button_StopTimer
            // 
            this.button_StopTimer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_StopTimer.Location = new System.Drawing.Point(85, 3);
            this.button_StopTimer.Name = "button_StopTimer";
            this.button_StopTimer.Size = new System.Drawing.Size(76, 78);
            this.button_StopTimer.TabIndex = 1;
            this.button_StopTimer.Text = "Stop";
            this.button_StopTimer.UseVisualStyleBackColor = true;
            this.button_StopTimer.Click += new System.EventHandler(this.button_StopTimer_Click);
            // 
            // button_ResetTimer
            // 
            this.button_ResetTimer.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_ResetTimer.Location = new System.Drawing.Point(167, 3);
            this.button_ResetTimer.Name = "button_ResetTimer";
            this.button_ResetTimer.Size = new System.Drawing.Size(76, 78);
            this.button_ResetTimer.TabIndex = 2;
            this.button_ResetTimer.Text = "Reset";
            this.button_ResetTimer.UseVisualStyleBackColor = true;
            this.button_ResetTimer.Click += new System.EventHandler(this.button_ResetTimer_Click);
            // 
            // button_Log
            // 
            this.button_Log.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_Log.Location = new System.Drawing.Point(249, 3);
            this.button_Log.Name = "button_Log";
            this.button_Log.Size = new System.Drawing.Size(76, 78);
            this.button_Log.TabIndex = 3;
            this.button_Log.Text = "Log";
            this.button_Log.UseVisualStyleBackColor = true;
            this.button_Log.Click += new System.EventHandler(this.button_Log_Click);
            // 
            // button_Settings
            // 
            this.button_Settings.Dock = System.Windows.Forms.DockStyle.Fill;
            this.button_Settings.Location = new System.Drawing.Point(331, 3);
            this.button_Settings.Name = "button_Settings";
            this.button_Settings.Size = new System.Drawing.Size(77, 78);
            this.button_Settings.TabIndex = 4;
            this.button_Settings.Text = "Settings";
            this.button_Settings.UseVisualStyleBackColor = true;
            this.button_Settings.Click += new System.EventHandler(this.button_Settings_Click);
            // 
            // panel_TaskList
            // 
            this.panel_TaskList.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel_TaskList.Location = new System.Drawing.Point(0, 146);
            this.panel_TaskList.Name = "panel_TaskList";
            this.panel_TaskList.Padding = new System.Windows.Forms.Padding(10);
            this.panel_TaskList.Size = new System.Drawing.Size(411, 381);
            this.panel_TaskList.TabIndex = 2;
            // 
            // MainView
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.ClientSize = new System.Drawing.Size(411, 527);
            this.Controls.Add(this.panel_TaskList);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.textBox_TotalElapsedTime);
            this.Name = "MainView";
            this.Text = "TaskTimer";
            this.Load += new System.EventHandler(this.Form1_Load);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.TextBox textBox_TotalElapsedTime;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Button button_StartTimer;
        private System.Windows.Forms.Button button_StopTimer;
        private System.Windows.Forms.Button button_ResetTimer;
        private System.Windows.Forms.Button button_Log;
        private System.Windows.Forms.Button button_Settings;
        private System.Windows.Forms.Panel panel_TaskList;
    }
}

