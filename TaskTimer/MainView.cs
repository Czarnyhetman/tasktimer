﻿using System;
using System.Collections.Generic;
using System.Windows.Forms;
using TaskTimerPresenter.View;
using TaskTimerPresenter.Data;
using TaskTimer.Controls;
using TaskTimerPresenter;

namespace TaskTimer
{
    public partial class MainView : Form, IMainView
    {
        public MainViewPresenter MainViewPresenter  { get; set; }

        public MainView()
        {
            InitializeComponent();
        }
        
        public void SetTotalTimeElapsed(string totalTimeElapsed)
        {
            if (InvokeRequired)
            {
                this.Invoke(new Action<string>(SetTotalTimeElapsed), new object[] { totalTimeElapsed });
                return;
            }
            textBox_TotalElapsedTime.Text = totalTimeElapsed;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            
        }

        #region ButtonClicks

        private void button_StartTimer_Click(object sender, EventArgs e)
        {
            MainViewPresenter.StartTimer();
        }

        private void button_StopTimer_Click(object sender, EventArgs e)
        {
            MainViewPresenter.StopTimer();
        }

        private void button_ResetTimer_Click(object sender, EventArgs e)
        {
            MainViewPresenter.ResetTimer();
        }

        private void button_Log_Click(object sender, EventArgs e)
        {
            
        }

        private void button_Settings_Click(object sender, EventArgs e)
        {

        }

        private void OnTaskSelection(object sender, EventArgs e)
        {
            ITaskView task = (ITaskView)sender;

            MainViewPresenter.SetActiveTask(task.Id);
        }


        //nowa funkcja dla aktualizacji
        public void SetTaskList(List<TaskData> tasks)
        {
            TableLayoutPanel tableLayout = new TableLayoutPanel();
            tableLayout.RowCount = tasks.Count;
            tableLayout.Dock = DockStyle.Fill;
            panel_TaskList.Controls.Add(tableLayout);

            int i = 0;
            foreach(TaskData task in tasks)
            {
                TaskControl taskControl = new TaskControl(task.Id, task.Name, task.Duration, task.Active);
                taskControl.Dock = DockStyle.Fill ;
                taskControl.OnClick += OnTaskSelection;
                tableLayout.Controls.Add(taskControl, 0, i++);
            }
        }

        #endregion
    }
}
