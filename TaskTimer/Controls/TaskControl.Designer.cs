﻿namespace TaskTimer.Controls
{
    partial class TaskControl
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.pictureBox_TaskStatus = new System.Windows.Forms.PictureBox();
            this.textBox_TaskName = new System.Windows.Forms.TextBox();
            this.textBox_TaskTime = new System.Windows.Forms.TextBox();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_TaskStatus)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 75F));
            this.tableLayoutPanel1.Controls.Add(this.pictureBox_TaskStatus, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.textBox_TaskName, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.textBox_TaskTime, 1, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(368, 87);
            this.tableLayoutPanel1.TabIndex = 0;
            this.tableLayoutPanel1.Click += new System.EventHandler(this.tableLayoutPanel1_Click);
            // 
            // pictureBox_TaskStatus
            // 
            this.pictureBox_TaskStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pictureBox_TaskStatus.Location = new System.Drawing.Point(10, 10);
            this.pictureBox_TaskStatus.Margin = new System.Windows.Forms.Padding(10);
            this.pictureBox_TaskStatus.Name = "pictureBox_TaskStatus";
            this.tableLayoutPanel1.SetRowSpan(this.pictureBox_TaskStatus, 2);
            this.pictureBox_TaskStatus.Size = new System.Drawing.Size(72, 67);
            this.pictureBox_TaskStatus.TabIndex = 0;
            this.pictureBox_TaskStatus.TabStop = false;
            this.pictureBox_TaskStatus.Paint += new System.Windows.Forms.PaintEventHandler(this.pictureBox_TaskStatus_Paint);
            // 
            // textBox_TaskName
            // 
            this.textBox_TaskName.Dock = System.Windows.Forms.DockStyle.Top;
            this.textBox_TaskName.Font = new System.Drawing.Font("Microsoft Sans Serif", 12.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox_TaskName.Location = new System.Drawing.Point(102, 10);
            this.textBox_TaskName.Margin = new System.Windows.Forms.Padding(10);
            this.textBox_TaskName.Name = "textBox_TaskName";
            this.textBox_TaskName.Size = new System.Drawing.Size(256, 26);
            this.textBox_TaskName.TabIndex = 1;
            // 
            // textBox_TaskTime
            // 
            this.textBox_TaskTime.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.textBox_TaskTime.Font = new System.Drawing.Font("Microsoft Sans Serif", 10.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(238)));
            this.textBox_TaskTime.Location = new System.Drawing.Point(102, 54);
            this.textBox_TaskTime.Margin = new System.Windows.Forms.Padding(10, 0, 10, 10);
            this.textBox_TaskTime.Name = "textBox_TaskTime";
            this.textBox_TaskTime.Size = new System.Drawing.Size(256, 23);
            this.textBox_TaskTime.TabIndex = 2;
            // 
            // TaskControl
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Margin = new System.Windows.Forms.Padding(10);
            this.MinimumSize = new System.Drawing.Size(300, 85);
            this.Name = "TaskControl";
            this.Size = new System.Drawing.Size(368, 87);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox_TaskStatus)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.PictureBox pictureBox_TaskStatus;
        private System.Windows.Forms.TextBox textBox_TaskName;
        private System.Windows.Forms.TextBox textBox_TaskTime;
    }
}
