﻿using System;
using System.Drawing;
using System.Windows.Forms;
using TaskTimerPresenter.View;

namespace TaskTimer.Controls
{
    public partial class TaskControl : UserControl, ITaskView
    {
        public int Id               { get; }
        public string TaskName      { get; set; }
        public string Duration      { get; set; }
        public bool Active          { get; set; }
        public EventHandler OnClick { get; set; }

        public TaskControl(int id, string name, string duration, bool active)
        {
            InitializeComponent();
            
            Id = id;
            TaskName = name;
            Duration = duration;
            Active = active;

            textBox_TaskName.Text = TaskName;
            textBox_TaskTime.Text = Duration;
        }

        private void pictureBox_TaskStatus_Paint(object sender, PaintEventArgs e)
        {
            Graphics g = e.Graphics;
            if (Active)
            {
                g.Clear(Color.Red);
            }
            else
            {
                g.Clear(Color.White);
            }
        }

        private void tableLayoutPanel1_Click(object sender, EventArgs e)
        {
            OnClick?.Invoke(this, new EventArgs());
        }
    }
}
