﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskTimerView
{
    interface ITotalTimeElapsedView
    {
        void SetTotalTimeElapsed(string totalTimeElapsed);
    }
}
