﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskTimerView
{
    public interface ITaskListView
    {
        void SetTaskList(List<TaskData> tasks);
        EventHandler OnTaskSelection { get; set; }
        EventHandler OnTaskRename { get; set; }
    }
}
