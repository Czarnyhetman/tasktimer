﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskTimerView
{
    public interface IStartTimerActionView
    {
        EventHandler OnStartTimerInvoked { get; set; }
    }
}
