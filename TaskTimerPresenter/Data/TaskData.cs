﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TaskTimerPresenter.Data
{
    public class TaskData
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Duration { get; set; }
        public bool Active { get; set; }

        public TaskData(int id, string name, string duration, bool active)
        {
            Id = id;
            Name = name;
            Duration = duration;
            Active = active;
        }
    }
}
