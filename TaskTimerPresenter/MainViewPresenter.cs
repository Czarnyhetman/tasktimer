﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TaskTimerModel.Interfaces;
using TaskTimerPresenter.Data;
using TaskTimerPresenter.View;

namespace TaskTimerPresenter
{
    public class MainViewPresenter
    {
        IMainView mainView;
        ITaskManager taskManager;

        public MainViewPresenter(IMainView mainView, ITaskManager taskManager)
        {
            this.mainView = mainView;
            this.taskManager = taskManager;
            mainView.MainViewPresenter = this;
            taskManager.Update += Update;

            InitializeTaskList();
        }

        #region Public interface

        public void ResetTimer()
        {
            taskManager.ResetTimer();
        }

        public void StartTimer()
        {
            taskManager.StartTimer();
        }

        public void StopTimer()
        {
            taskManager.StopTimer();
        }

        public void SetActiveTask(int taskId)
        {
            taskManager.SetActiveTask(taskId);
            List<TaskData> taskList = BuildTaskDataList(taskManager.TaskList, taskManager.ActiveTaskId);
            mainView.SetTaskList(taskList);
        }

        #endregion

        private void Update(object sender, EventArgs e)
        {
            mainView.SetTotalTimeElapsed(taskManager.TotalTimeElapsed.ToString());
        }

        private void InitializeTaskList()
        {
            List<TaskData> taskList = BuildTaskDataList(taskManager.TaskList, taskManager.ActiveTaskId);
            mainView.SetTaskList(taskList);
        }

        private List<TaskData> BuildTaskDataList(List<TaskTimerModel.Task> taskList, int activeTaskId)
        {
            List<TaskData> taskDataList = new List<TaskData>();

            foreach (TaskTimerModel.Task task in taskList)
            {
                bool active = task.Id == activeTaskId;
                TaskData taskData = new TaskData(
                    task.Id,
                    task.Name,
                    task.OverallDuration.ToString(),
                    active
                    );
                taskDataList.Add(taskData);
            }
            return taskDataList;
        }
    }
}
