﻿using System;

namespace TaskTimerPresenter.View
{
    public interface ITaskView
    {
        int Id { get;}
        string Name { get; set; }
        string Duration { get; set; }
    }
}
