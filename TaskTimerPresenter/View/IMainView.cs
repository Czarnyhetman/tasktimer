﻿using System.Collections.Generic;
using TaskTimerPresenter.Data;

namespace TaskTimerPresenter.View
{
    public interface IMainView
    {
        MainViewPresenter MainViewPresenter { get; set; }

        void SetTotalTimeElapsed(string totalTimeElapsed);
        void SetTaskList(List<TaskData> tasks);
    }
}
